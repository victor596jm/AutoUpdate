﻿using AutoUpdateLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                AutoUpdate.InvokeUpdate("Test.exe", "Test", StartMain, args);
            }
            catch (ApplicationAlreadRunException ex)
            {
                MessageBox.Show( ex.Message, "Error");
                Application.Exit();
                return;
            }
            catch (AutoUpdateException ex)
            {
                MessageBox.Show(ex.Message, "Error");
                StartMain(args);
            }


        }
        private static void StartMain(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
