﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                ProgramRunned(@"E:\YISHION\NewRetail\bin\ServiceStackHosting.exe", true, false).ToString());
        }
        /// <summary>
        /// 取去除文件扩展名之后的文件字符串
        /// </summary>
        /// <param name="Prog"></param>
        /// <returns></returns>
        private static string GetShortExec(string Prog)
        {
            int Pos = Prog.LastIndexOf(".");
            return (Pos > 0 ? Prog.Substring(0, Pos) : Prog);
        }
        /// <summary>
        /// 取程序名(去除路径，扩展名)
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string GetFileNameOnly(string fileName)
        {
            return GetShortExec(new System.IO.FileInfo(fileName).Name);
        }
        /// <summary>
        /// 返回进程id, c:\windows\system32\Notepad.exe,notepad.exe
        /// </summary>
        /// <param name="ProgFullName"></param>
        /// <param name="Kill"></param>
        /// <returns></returns>
        public static int ProgramRunned(string ProgName, bool isFullName = false, bool Kill = false)
        {
            string Prog = isFullName ?
                GetFileNameOnly(ProgName)
                : GetShortExec(ProgName);
            System.Diagnostics.Process[] mProcs =
                System.Diagnostics.Process.GetProcessesByName(Prog);
            if (0 == (mProcs?.Length ?? 0)) return 0;
            int pid = 0;
            for (int i = 0, Count = mProcs.Length; i < Count; i++)
            {
                System.Diagnostics.Process proc = mProcs[i];
                bool exists = false;
                if (isFullName)
                    exists = ProgName.Equals(proc.MainModule.FileName, StringComparison.OrdinalIgnoreCase);
                else
                    exists = proc.ProcessName.Equals(ProgName, StringComparison.OrdinalIgnoreCase);
                if (exists)
                {
                    pid = proc.Id;
                    if (Kill)
                    {
                        proc.Kill();
                    }
                }
            }
            return pid;
        }
    }
}
