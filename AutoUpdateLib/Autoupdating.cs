﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace AutoUpdateLib
{
    public class Autoupdating
    {
        readonly string _limitFile = "limit.xml";
        readonly static string _updateListFile ="updatelist.xml";
        readonly string _userconfigFile = "userconfig.xml";
        readonly static string _appDir = Common.AppDir;
        readonly string _tempDir;
        readonly static XmlFiles _updateXmlFile;
        internal static bool NeedUpdate { get; private set; }
        private string _updateUrl = "";
        readonly static AutoUpdateLog _logger = AutoUpdateLog.LogObj;
        static Autoupdating()
        {
            string xmlFileName = Common.CombineDir(_appDir, _updateListFile);
            if (Common.FileExists(xmlFileName))
            {
                _updateXmlFile = new XmlFiles(xmlFileName);
            }
            NeedUpdate =_updateXmlFile.GetNodeValue("//NeedUpdate").IsTrue();
            _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,NeedUpdate?{NeedUpdate}");
        }
        public static string GetVersion()
        {
            return _updateXmlFile.GetNodeValue(@"/AutoUpdater/Application/Version");
        }
        public static string GetVersionSort()
        {
            return _updateXmlFile.GetNodeValue(@"/AutoUpdater/Application/VersionSort");
        }
        public static string GetLocalFileVersion(string fileName)
        {
            XmlNodeList xnList = _updateXmlFile.SelectNodes($"/AutoUpdater/Files/File[@Name='{fileName}']");
            if (0 < xnList.Count)
                return xnList[0].Attributes["Ver"].InnerText;
            return string.Empty;
        }

        public Autoupdating()
        {
            _tempDir = Common.CombineDir(_appDir, "temp"); //临时下载目录
        }
        private void GetDownloadUrl()
        {
            if (!NeedUpdate) return;
            _updateUrl = _updateXmlFile.GetNodeValue("//Url");
            if(string.IsNullOrWhiteSpace(_updateUrl))
                _updateUrl = _updateXmlFile.GetNodeValue("//Url1");
            if (string.IsNullOrWhiteSpace(_updateUrl))
                _updateUrl = _updateXmlFile.GetNodeValue("//Url2");
            if (string.IsNullOrWhiteSpace(_updateUrl))
            {
                _updateUrl = XPathGet.GetValue(Common.CombineDir(_appDir, _userconfigFile), "//FIXXED_UPDATE_URL", "");
            }
        }
        private void CheckAndDownNewerAutoUpdate()
        {
            _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,UpdateUrl?{_updateUrl}");
            if (string.IsNullOrWhiteSpace(_updateUrl))
                throw new ArgumentNullException("UpdateUrl");
            _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,Download file ?{_limitFile}");
            WebOp.Download(_updateUrl, _limitFile, _tempDir);
            string limitFileOfServer = Common.CombineDir(_tempDir, _limitFile);
            if (!Common.FileExists(limitFileOfServer))
            {
                _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,File {limitFileOfServer} doesn't exists.");
                throw new System.IO.FileNotFoundException(limitFileOfServer);
            }
            string versionInServer = XPathGet.GetValue(limitFileOfServer, "//AutoupdateVersion", ""); //Autoupdate.exe在服务器的版本
            string versionInLocal = GetLocalFileVersion("Autoupdate.exe");
            _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,Autoupdate.exe version in local:{versionInLocal},remote:{versionInServer}");
            bool autoupdateIsNew = versionInServer.CompareTo(versionInLocal) != 0;
            if (autoupdateIsNew)
            {
                _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,Download Autoupdate.exe...");
                WebOp.Download(_updateUrl, "Autoupdate.exe", _appDir);
                _logger?.AddLog(LOG_MODE.INFO, $"Autoupdating,Finished.");
            }
        }
        public void CheckAndDownAutoUpdateFile()
        {
            GetDownloadUrl();
            CheckAndDownNewerAutoUpdate();
        }
        public void InvokeAutoUpdate(string[] args=null)
        {
            string AutoUptCmd = Common.CombineDir(_appDir, "AutoUpdate.exe");
            if (!Common.FileExists(AutoUptCmd)|| !NeedUpdate) return;
            CheckAndDownAutoUpdateFile();
            _logger?.Dispose();
            System.Threading.Thread.Sleep(1000);
            List<string> arg = new List<string>() { "--CALLFROMPROG" }; //调用自动更新程序加参数
            if (null != args) arg.AddRange(args);
            Common.BeginProcess(AutoUptCmd,string.Join(" ", arg), _appDir,hiddenWindow:false);
        }
    }
}
