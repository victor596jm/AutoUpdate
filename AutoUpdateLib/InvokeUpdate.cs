﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace AutoUpdateLib
{
    public class ApplicationAlreadRunException : Exception
    {
        public string ProgramName { get; private set; }
        public ApplicationAlreadRunException(string progName,string errorText) : base(errorText)
        {
            this.ProgramName = progName;
        }
    }
    public class AutoUpdateException : Exception
    {
        public string ProgramName { get; private set; }
        public AutoUpdateException(string progName,Exception ex) : base(ex.Message)
        {
            this.ProgramName = progName;
        }
    }
    public static class AutoUpdate
    {
        public static void InvokeUpdate(string progName,string mutexName,Action<string[]> mainFunction,string[] args)
        {
            bool allowMulti = string.IsNullOrWhiteSpace(mutexName); //允许多进程
            bool MainProgRunned = true;
            bool grantedOwnership= true;
            string fullProgName = Common.CombineDir(Common.AppDir, progName);
            MainProgRunned = Common.ProgramRunned(fullProgName, true, false) > 0; //进程是否启动
            Mutex singleInstanceMutex = null;
            if( !allowMulti)
                singleInstanceMutex = new Mutex(true, mutexName, out grantedOwnership);
            try
            {
                if (!grantedOwnership)
                {
                    throw new ApplicationAlreadRunException(progName,$"{progName}已经运行!");
                }
                else
                {
                    bool noupdate = null != args && args.Contains("--NOUPDATE", StringCompareNoCase.Instance);
                    //以下语句启动自动更新功能
                    if (!noupdate && Autoupdating.NeedUpdate)
                    {
                        try
                        {
                            string curProcess = Common.GetCurrentProgName();
                            if (curProcess.IndexOf("VSHOST", StringComparison.OrdinalIgnoreCase) < 0)
                            {
                                new Autoupdating().InvokeAutoUpdate();
                            }
                        }
                        catch (Exception ex)
                        {
                            throw new AutoUpdateException(progName,ex );
                        }
                    }
                    //启动主界面
                    Common.ProgramRunned(Common.CombineDir(Common.AppDir,"AutoUpate.exe"),true, true);
                    mainFunction?.Invoke(args);
                }
            }
            catch (Exception ex)
            {
                throw new AutoUpdateException(progName, ex);
            }
            finally
            {
                singleInstanceMutex?.Close();
            }
        }
    }
}
