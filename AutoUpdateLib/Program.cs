﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
namespace AutoUpdateLib
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] Args)
        {
            try
            {
                AutoUpdate.InvokeUpdate("AutoUpdateLib.exe", "AutoUpdateLib",null, null);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
