﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace AutoUpdateLib
{
    class StringCompareNoCase : IEqualityComparer<string>
    {
        public bool Equals(string t1, string t2)
        {
            return t1.Equals(t2, StringComparison.OrdinalIgnoreCase);
        }
        public int GetHashCode(string obj)
        {
            return obj.ToString().ToLower().GetHashCode();
        }
        public readonly static StringCompareNoCase Instance = new StringCompareNoCase();
    }
    internal static class Common
    {
        /// <summary>
        /// 是否以文件共享的方式
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public static bool IsFileShare(this string Url) =>
            !Url.IsHttp() && !Url.IsFtp();
        public static bool IsHttp(this string url)
            => url?.StartsWith("http", StringComparison.OrdinalIgnoreCase) ?? false;
        public static bool IsFtp(this string url)
            => url?.StartsWith("ftp:", StringComparison.OrdinalIgnoreCase) ?? false;
        public static bool IsTrue(this string str)
        {
            return !string.IsNullOrEmpty(str) &&
                ("Y".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "YES".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "1".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "true".Equals(str, StringComparison.OrdinalIgnoreCase));
        }
#if DOTNET35
        public static string CombineDir(string Para1,string Para2)
        {
            return System.IO.Path.Combine(Para1,Para2);
        }
#else
        public static string CombineDir(params string[] Para)
        {
            return System.IO.Path.Combine(Para);
        }
#endif
        public static void CheckFolderExists(string Folder)
        {
            if (!(System.IO.Directory.Exists(Folder)))
                System.IO.Directory.CreateDirectory(Folder);
        }
        public static string GetApplicationBaseDir()
        {
            string appDir = AppDomain.CurrentDomain.BaseDirectory;
            if (!appDir.EndsWith("\\")) appDir += "\\";
            return appDir;
        }
        public static bool FileExists(string FileName)
        {
            return System.IO.File.Exists(FileName);
        }
        public readonly static string AppDir = GetApplicationBaseDir();
        /// <summary>
        /// 处理文件目录
        /// </summary>
        /// <param name="dir"></param>
        public static void DealFileDirect(ref string dir) => dir = dir.Replace('/', '\\');
        public static string CombileUrl(string baseUrl, string shortFileName)
        {
            bool isfileShare = baseUrl.IsFileShare();
            string result = baseUrl;
            if (isfileShare)
            {
                if (!result.EndsWith(@"\")) result += @"\";
                result += shortFileName;
                result = result.Replace('/', '\\');
            }
            else
            {
                if (!result.EndsWith(@"/")) result += @"/";
                result += shortFileName;
                result = result.Replace('\\', '/');
            }
            return result;
        }
        /// <summary>
        /// 取去除文件扩展名之后的文件字符串
        /// </summary>
        /// <param name="Prog"></param>
        /// <returns></returns>
        private static string GetShortExec(string Prog)
        {
            int Pos = Prog.LastIndexOf(".");
            return (Pos > 0 ? Prog.Substring(0, Pos) : Prog);
        }
        /// <summary>
        /// 取程序名(去除路径，扩展名)
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string GetFileNameOnly(string fileName)
        {
            return GetShortExec(new System.IO.FileInfo(fileName).Name);
        }
        public static string GetCurrentProgName() => Process.GetCurrentProcess().ProcessName;
        public static Process BeginProcess(string ProgFullName, string Args, string workDir = "", bool autoStart = true,
   bool hiddenWindow = true, bool useCmdExe = false, bool useShellExe = false, bool waitforExit = false)
        {
            if (waitforExit && !autoStart)
                throw new ArgumentException("waitforExit/autoStart");
            Process p = new Process();
            string progName = useCmdExe ? "cmd.exe" : ProgFullName, args = "";
            args = useCmdExe ? $@" /C ""start {ProgFullName}""" : Args;
            p.StartInfo.FileName = progName;
            if (!string.IsNullOrWhiteSpace(workDir))
                p.StartInfo.WorkingDirectory = workDir;
            if (!string.IsNullOrWhiteSpace(args))
                p.StartInfo.Arguments = args;
            if (useShellExe)
                p.StartInfo.UseShellExecute = true;
            if (hiddenWindow)
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            if (autoStart) p.Start();
            if (waitforExit) p.WaitForExit();
            return p;
        }
        /// <summary>
        /// 返回进程id, c:\windows\system32\Notepad.exe,notepad.exe
        /// </summary>
        /// <param name="ProgFullName"></param>
        /// <param name="Kill"></param>
        /// <returns></returns>
        public static int ProgramRunned(string ProgName, bool isFullName = false, bool Kill = false)
        {
            string Prog = isFullName ?
                GetFileNameOnly(ProgName)
                : GetShortExec(ProgName);
            System.Diagnostics.Process[] mProcs =
                System.Diagnostics.Process.GetProcessesByName(Prog);
            if (0 == (mProcs?.Length ?? 0)) return 0;
            int pid = 0;
            for (int i = 0, Count = mProcs.Length; i < Count; i++)
            {
                System.Diagnostics.Process proc = mProcs[i];
                bool exists = false;
                if (isFullName)
                    exists = ProgName.Equals(proc.MainModule.FileName, StringComparison.OrdinalIgnoreCase);
                else
                    exists = proc.ProcessName.Equals(ProgName, StringComparison.OrdinalIgnoreCase);
                if (exists)
                {
                    pid = proc.Id;
                    if (Kill)
                    {
                        proc.Kill();
                    }
                }
            }
            return pid;
        }
    }
}
