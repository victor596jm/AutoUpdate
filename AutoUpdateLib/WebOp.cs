﻿using System;
using System.IO;
using System.Web;
using System.Net;
namespace AutoUpdateLib
{
    internal static  class WebOp
    {
        /// <summary>   
        /// 将 Stream 写入文件   
        /// </summary>   
        public static void StreamToFile(this Stream stream, string filepath,
            int bufferSize = 8192, bool isNetworkStream = false, Action<long> handerWriteBytes = null)
        {
            // 把 byte[] 写入文件   
            FileStream fs = null;
            try
            {
                fs = new FileStream(filepath, FileMode.Create);
                if (!isNetworkStream)
                {
                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, bytes.Length);
                    // 设置当前流的位置为流的开始   
                    stream.Seek(0, SeekOrigin.Begin);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(bytes);
                    bw?.Close();
                }
                else
                {
                    byte[] buffer = new byte[bufferSize];
                    long totalWriteBytes = 0;
                    int readCount;
                    readCount = stream.Read(buffer, 0, bufferSize);//从ftp的responseStream读取数据到buffer中
                    while (readCount > 0)
                    {
                        fs.Write(buffer, 0, readCount);//从buffer读取数据到fileStream中，完成下载
                        totalWriteBytes += readCount;
                        handerWriteBytes?.Invoke(totalWriteBytes);
                        readCount = stream.Read(buffer, 0, bufferSize);
                    }
                }
            }
            finally
            {
                fs?.Close();
                fs?.Dispose();
            }
        }
        public static void Download(string baseUrl, string shortFileName, string localPath)
        {
            if (!Directory.Exists(localPath))
                Directory.CreateDirectory(localPath);
            string fileName = Common.CombineDir(localPath, shortFileName);
            Common.DealFileDirect(ref fileName);
            if (System.IO.File.Exists(fileName))
            {
                try
                {
                    System.IO.File.Delete(fileName);
                }
                catch { }
            }
            string url = Common.CombileUrl(baseUrl, shortFileName);
            if (url.IsHttp())
            {
                WebRequest webReq = WebRequest.Create(url);
                webReq.UseDefaultCredentials = true;
                WebResponse webRes = webReq.GetResponse();
                long fileLength = webRes.ContentLength; //ftp取不到值
                Stream downStream = webRes.GetResponseStream();
                downStream.StreamToFile(fileName, isNetworkStream: true);
            }
            else
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.UseBinary = true;
                FtpWebResponse response = null;
                try
                {
                    response = (FtpWebResponse)request.GetResponse();
                    response.GetResponseStream().StreamToFile(fileName, isNetworkStream: true);
                }
                finally
                {
                    response?.Close();
                }
            }
        }
    }
}
