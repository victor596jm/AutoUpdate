﻿using System;
using System.Collections.Generic;
using System.Text;
namespace BIN
{
    class LocalFileVersion
    {
        public string FileName = "";
        public string Version = "";
        public LocalFileVersion(string fileName, string verSion)
        {
            FileName = fileName;
            Version = verSion;
        }
    }
    class RenameFile
    {
        public string OriFileName = string.Empty;
        public string NewFileName = string.Empty;
        public RenameFile(string oriFileName, string newFileName)
        {
            OriFileName = oriFileName;
            NewFileName = newFileName;
        }
    }
}
