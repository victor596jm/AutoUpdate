﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;

namespace BIN
{
    enum DATEPART
    {
        DAY,
        HOUR,
        MINUTE,
        SECOND
    }
    class StringCompareNoCase : IEqualityComparer<string>
    {
        public bool Equals(string t1, string t2)
        {
            return t1.Equals(t2, StringComparison.OrdinalIgnoreCase);
        }
        public int GetHashCode(string obj)
        {
            return obj.ToString().ToLower().GetHashCode();
        }
        public readonly static StringCompareNoCase Instance = new StringCompareNoCase();
    }
    class UpdateFileInfo
    {
        public string FileName { get; set; }
        public string Version { get; set; }
        public string Progress { get; set; }
        public string IsKey { get; set; }
    }
    internal static class Common
    {
        public static UpdateFileInfo ToUpdateFileInfo(this string[] info)
        {
            return new UpdateFileInfo
            {
                FileName = info[0],
                Version = info[1],
                Progress = info[2],
                IsKey = info[3]
            };
        }
        public static bool IsTrue(this string str)
        {
            return !string.IsNullOrEmpty(str) &&
                ("Y".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "YES".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "1".Equals(str, StringComparison.OrdinalIgnoreCase)
                || "true".Equals(str, StringComparison.OrdinalIgnoreCase));
        }
        private static bool _callfromMainProg = false;
        /// <summary>
        /// 是否从主程序调用
        /// </summary>
        public static bool CallFromMainProg
        {
            get { return _callfromMainProg; }
            set { _callfromMainProg = value; }
        }
        /// <summary>
        /// 主程序的参数
        /// </summary>
        internal static string[] MainProgArgs { get; set; }
        internal static string GetMainProgArgsNoUpdate()
        {
            List<string> args = new List<string>();
            if (0 < (MainProgArgs?.Length ?? 0)) args.AddRange(MainProgArgs);
            args.Add("--NOUPDATE");
            return string.Join(" ", args);
        }
        private static readonly List<string> _ExceptFileFixxed
            = new List<string>()
            {
             "AutoUpdate.exe","UserConfig.xml","Limit.xml"
            };
        /// <summary>
        /// 固定排除的升级文件
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool FileIsFixedExcepted(string fileName)
        {
            return _ExceptFileFixxed.Contains(fileName, StringCompareNoCase.Instance);
        }
        public static void CheckDir()
        {
            string appDir = Common.GetAppDir();
            string tmpDir = Common.CombineDir(appDir, "temp");
            CheckFolderExists(tmpDir);
            tmpDir = Common.CombineDir(appDir, "Logs");
            CheckFolderExists(tmpDir);
        }
        /// <summary>
        /// 是否以文件共享的方式
        /// </summary>
        /// <param name="Url"></param>
        /// <returns></returns>
        public static bool IsFileShare(this string Url) =>
            !Url.IsHttp() && !Url.IsFtp();
        public static bool IsHttp(this string url)
            => url?.StartsWith("http", StringComparison.OrdinalIgnoreCase) ?? false;
        public static bool IsFtp(this string url)
            => url?.StartsWith("ftp:", StringComparison.OrdinalIgnoreCase) ?? false;
        public static string CombileUrl(string baseUrl, string shortFileName)
        {
            bool isfileShare = baseUrl.IsFileShare();
            string result = baseUrl;
            if (isfileShare)
            {
                if (!result.EndsWith(@"\")) result += @"\";
                result += shortFileName;
                result = result.Replace('/', '\\');
            }
            else
            {
                if (!result.EndsWith(@"/")) result += @"/";
                result += shortFileName;
                result = result.Replace('\\', '/');
            }
            return result;
        }
        /// <summary>
        /// 处理文件目录
        /// </summary>
        /// <param name="dir"></param>
        public static void DealFileDirect(ref string dir) => dir = dir.Replace('/', '\\');
        public readonly static clsConfig Config = new clsConfig();
        public static void CheckFolderExists(string Folder)
        {
            if (!System.IO.Directory.Exists(Folder))
                System.IO.Directory.CreateDirectory(Folder);
        }
        public static Process BeginProcess(string ProgFullName, string Args, string workDir = "", bool autoStart = true,
           bool hiddenWindow = false, bool useCmdExe = false, bool useShellExe = false, bool waitforExit = false)
        {
            if (waitforExit && !autoStart)
                throw new ArgumentException("waitforExit/autoStart");
            Process p = new Process();
            string progName = useCmdExe ? "cmd.exe" : ProgFullName, args = "";
            args = useCmdExe ? $@" /C ""start {ProgFullName}""" : Args;
            p.StartInfo.FileName = progName;
            if (!string.IsNullOrWhiteSpace(workDir))
                p.StartInfo.WorkingDirectory = workDir;
            if (!string.IsNullOrWhiteSpace(args))
                p.StartInfo.Arguments = args;
            if (useShellExe)
                p.StartInfo.UseShellExecute = true;
            if (hiddenWindow)
                p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            if (autoStart) p.Start();
            if (waitforExit) p.WaitForExit();
            return p;
        }
        public static bool UpdateListFileIsOk(string FullFileName)
        {
            if (!Common.FileExists(FullFileName)) return false;
            try
            {
                FileInfo fi = new FileInfo(FullFileName);
                return (Math.Abs(Common.DateDiff(DATEPART.SECOND,
                    fi.LastWriteTime, DateTime.Now)) < 60);
            }
            catch
            {
                return false;
            }
        }
        public static bool IsUpdateAgent
        {
            get
            {
                try
                {
                    return Config[CONFIG_KEY.IS_UPDATE_AGENT].IsTrue();
                }
                catch
                {
                    return false;
                }
            }
        }
        private static string _updateKey = string.Empty;
        /// <summary>
        /// 升级关键字如AGD
        /// </summary>
        public static string UpdateKey
        {
            get
            {
                if (string.IsNullOrEmpty(_updateKey))
                {
                    try
                    {
                        _updateKey = Config[CONFIG_KEY.UPDATE_KEY];
                    }
                    catch
                    {
                        _updateKey = string.Empty;
                    }
                }
                return _updateKey;
            }
        }
        public static string ShareFolder
        {
            get
            {
                return Config[CONFIG_KEY.SHARE_FOLDER].Trim();
            }
        }
        public static bool OnlyKeepMe => Config[CONFIG_KEY.ONLY_KEEP_ME].IsTrue();
        public static string GetAppDir()
        {
            string appDir = System.AppDomain.CurrentDomain.BaseDirectory;
            if (!appDir.EndsWith("\\")) appDir += "\\";
            return appDir;
        }
        public static string GetObjectString(object obj)
        {
            return ((obj == null || obj == DBNull.Value
                || string.IsNullOrEmpty(obj.ToString())) ? string.Empty : obj.ToString());
        }
        public static bool FileExists(string FileName)
        {
            return (System.IO.File.Exists(FileName));
        }
        public static string GetShortFileName(string FileName)
        {
            int Index = FileName.LastIndexOf('\\');
            if (Index < 0) return FileName;
            return FileName.Substring(Index + 1, FileName.Length - Index - 1);
        }
        private static string GetShortExec(string Prog)
        {
            int Pos = Prog.LastIndexOf(".");
            return Pos > 0 ? Prog.Substring(0, Pos) : Prog;
        }
        /*
        public static string CombineDir(params string[] Para)
        {
            return System.IO.Path.Combine(Para);
        }
        */
        public static string CombineDir(string dir1, string dir2)
        {
            return System.IO.Path.Combine(dir1, dir2);
        }
        public static string CombineDir(string dir1, string dir2, string dir3)
        {
            return CombineDir(CombineDir(dir1, dir2), dir3);
        }
        public static double DateDiff(DATEPART dp, DateTime dt1, DateTime dt2)
        {
            TimeSpan t1 = new TimeSpan(dt1.Ticks);
            TimeSpan t2 = new TimeSpan(dt2.Ticks);
            TimeSpan Diff = t1.Subtract(t2).Duration();
            double Ret = 0;
            switch (dp)
            {
                case DATEPART.DAY:
                    Ret = (Diff.TotalDays);
                    break;
                case DATEPART.HOUR:
                    Ret = (Diff.TotalHours);
                    break;
                case DATEPART.MINUTE:
                    Ret = (Diff.TotalMinutes);
                    break;
                case DATEPART.SECOND:
                    Ret = (Diff.TotalSeconds);
                    break;
                default:
                    Ret = 0.0;
                    break;
            }
            bool Little = (dt2 < dt1);
            //return Ret;
            return Little ? Ret * -1 : Ret;
        }
        /// <summary>
        /// 取程序名(去除路径，扩展名)
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        private static string GetFileNameOnly(string fileName)
        {
            return GetShortExec(new System.IO.FileInfo(fileName).Name);
        }
        /// <summary>
        /// 文件名是否相同
        /// </summary>
        /// <param name="fileName1"></param>
        /// <param name="fileName2"></param>
        /// <returns></returns>
        public static bool FileNameIsSame(string fileName1, string fileName2)
        {
            return 0 == string.Compare(fileName1, fileName2, true);
        }
        public static bool IsProgExists(int ProgId, bool kill = false)
        {
            Process proc = Process.GetProcessById(ProgId);
            bool runned = null != proc;
            if (kill) proc.Kill();
            return runned;
        }
        /// <summary>
        /// 返回进程id, c:\windows\system32\Notepad.exe,notepad.exe
        /// </summary>
        /// <param name="ProgFullName"></param>
        /// <param name="Kill"></param>
        /// <returns></returns>
        public static int ProgramRunned(string ProgName, bool isFullName = false, bool Kill = false)
        {
            string Prog = isFullName ?
                 GetFileNameOnly(ProgName)
                : GetShortExec(ProgName);
            System.Diagnostics.Process[] mProcs =
                System.Diagnostics.Process.GetProcessesByName(Prog);
            if (0 == (mProcs?.Length ?? 0)) return 0;
            int pid = 0;
            for (int i = 0, Count = mProcs.Length; i < Count; i++)
            {
                System.Diagnostics.Process proc = mProcs[i];
                bool exists = false;
                if (isFullName)
                    exists = ProgName.Equals(proc.MainModule.FileName, StringComparison.OrdinalIgnoreCase);
                else
                    exists = proc.ProcessName.Equals(ProgName, StringComparison.OrdinalIgnoreCase);
                if (exists)
                {
                    pid = proc.Id;
                    if (Kill)
                    {
                        proc.Kill();
                    }
                }
            }
            return pid;
        }
        /// <summary>
        /// 取同一程序打开的进程的数量 notepad.exe
        /// </summary>
        /// <param name="Prog"></param>
        /// <returns></returns>
        public static int GetProcessCount(string Prog)
        {
            string prog = GetShortExec(Prog);
            System.Diagnostics.Process[] mProcs =
                System.Diagnostics.Process.GetProcessesByName(prog);
            if (null == mProcs || 0 == mProcs.Length)
                return 0;
            else
                return mProcs.Length;
        }
        public static string GetCurrentProgName()
            => System.Diagnostics.Process.GetCurrentProcess().ProcessName;
        /// <summary>
        /// notepad,notepad.exe
        /// </summary>
        /// <param name="Exe"></param>
        /// <returns></returns>
        public static bool ProgInMemory(string Exe)
        {
            return System.Diagnostics.Process.GetProcessesByName(GetShortExec(Exe)).Length > 0;
        }
        public static bool ProgRunnedExceptPid(string Exe, int Id)
        {
            System.Diagnostics.Process[] pces =
          System.Diagnostics.Process.GetProcessesByName(GetShortExec(Exe));
            int count = pces.Length;
            for (int i = 0; i < count; i++)
            {
                if (pces[i].Id == Id) continue;
                return true;
            }
            return false;
        }
        /// <summary>   
        /// 将 Stream 写入文件   
        /// </summary>   
        public static void StreamToFile(this Stream stream, string filepath,
            int bufferSize=8192, bool isNetworkStream=false,Action<long> handerWriteBytes=null)
        {
            // 把 byte[] 写入文件   
            FileStream fs = null;
            try
            {
                fs = new FileStream(filepath, FileMode.Create);
                if (!isNetworkStream)
                {
                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, bytes.Length);
                    // 设置当前流的位置为流的开始   
                    stream.Seek(0, SeekOrigin.Begin);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(bytes);
                    bw?.Close();
                }
                else
                {
                    byte[] buffer = new byte[bufferSize];
                    long totalWriteBytes = 0;
                    int readCount;
                    readCount = stream.Read(buffer, 0, bufferSize);//从ftp的responseStream读取数据到buffer中
                    while (readCount > 0)
                    {
                        fs.Write(buffer, 0, readCount);//从buffer读取数据到fileStream中，完成下载
                        totalWriteBytes += readCount;
                        handerWriteBytes?.Invoke(totalWriteBytes);
                        readCount = stream.Read(buffer, 0, bufferSize);
                    }
                }
            }
            finally
            {
                fs?.Close();
                fs?.Dispose();
            }
        }
        public static void Download(string baseUrl, string shortFileName, string localPath)
        {
            if (!Directory.Exists(localPath))
                Directory.CreateDirectory(localPath);
            string fileName = Common.CombineDir(localPath, shortFileName);
            DealFileDirect(ref fileName);
            if (System.IO.File.Exists(fileName))
            {
                try
                {
                    System.IO.File.Delete(fileName);
                }
                catch { }
            }
            string url = CombileUrl(baseUrl, shortFileName);
            if (url.IsHttp())
            {
                /* ok
                WebClient req = null;
                try
                {
                    req = new WebClient();
                    req.UseDefaultCredentials = true;
                    req.DownloadFile(url, fileName);
                }
                finally
                {
                    req?.Dispose();
                }
                */
                WebRequest webReq = WebRequest.Create(url);
                webReq.UseDefaultCredentials = true;
                WebResponse webRes = webReq.GetResponse();
                long fileLength = webRes.ContentLength; //ftp取不到值
                Stream downStream = webRes.GetResponseStream();
                downStream.StreamToFile(fileName,isNetworkStream:true);
                /* ok
                StreamReader srmReader = null;
                FileStream fs = null;
                try
                {
                    srmReader = new StreamReader(downStream);
                    byte[] bufferbyte = new byte[fileLength];
                    int allByte = (int)bufferbyte.Length;
                    int startByte = 0;
                    while (fileLength > 0)
                    {
                        int downByte = downStream.Read(bufferbyte, startByte, allByte);
                        if (downByte == 0) break;
                        startByte += downByte;
                        allByte -= downByte;
                    }
                    fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Write);
                    fs.Write(bufferbyte, 0, bufferbyte.Length);
                }
                finally
                {
                    srmReader?.Close();
                    fs?.Close();
                    fs?.Dispose();
                    srmReader?.Dispose();
                }
                */
            }
            else
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.UseBinary = true;
                FtpWebResponse response = null;
                try
                {
                    response = (FtpWebResponse)request.GetResponse();
                    response.GetResponseStream().StreamToFile(fileName, isNetworkStream: true);
                }
                finally
                {
                    response?.Close();
                }
            }
        }
        public static string SplitFtpUrl(string ftpUrl, out string user, out string password)
        {
            if (!ftpUrl?.StartsWith("ftp:", StringComparison.OrdinalIgnoreCase) ?? false)
                throw new ArgumentException("ftp url");
            string[] segs = ftpUrl.Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);// ftp //yishion@163.com yishion@localhost
            if (3 != (segs?.Length ?? 0)) throw new ArgumentException("ftp url");
            user = segs[1].TrimStart(new char[] { '/' });
            string[] segs2 = segs[2].Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
            if (2 != (segs2?.Length ?? 0)) throw new ArgumentException("ftp url");
            password = segs2[0];
            return $"ftp://{segs2[1]}";
        }
    }
}
