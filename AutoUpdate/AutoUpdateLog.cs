﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
namespace BIN
{
    class AutoUpdateLog
    {
        private StreamWriter _SW = null;
        public AutoUpdateLog(string FileName)
        {
            try
            {
                FileInfo fi = null;
                bool existsFile = Common.FileExists(FileName);
                if (!existsFile)
                {
                    fi = new FileInfo(FileName);
                    if (!fi.Directory.Exists)
                        Directory.CreateDirectory(fi.Directory.ToString());
                }
                if(!Common.CallFromMainProg)
                    _SW = new StreamWriter(FileName, false);
                else
                    _SW = new StreamWriter(FileName, true);
                _SW.AutoFlush = true;
            }
            catch { }
        }
        public void Dispose()
        {
            _SW = null;
        }
        ~AutoUpdateLog()
        {
            this.Dispose();
        }
        public void AddLog(LOG_MODE LogMode, string LogText)
        {
            if (_SW == null) return;
            try
            {
                _SW.WriteLine(string.Format("{0} {1} {2}", DateTime.Now.ToString("yyMMdd HH:mm:ss"),
                    LogMode == LOG_MODE.INFO ? "info" :
                    (LogMode == LOG_MODE.WARN ? "warn" : "error"), LogText));
            }
            catch { }
        }
        public static readonly AutoUpdateLog LogObj =
            new AutoUpdateLog(Common.CombineDir( Common.GetAppDir(),"Logs","UpdateLog.log"));
    }
}
