﻿using System;
using System.Linq;
using System.Threading;
using System.Collections.Generic;
#if !CONSOLE_MODE
using System.Windows.Forms;
#endif
namespace BIN
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main(string[] Args)
        {
            string mutexName = "UPTAPP";
            bool grantedOwnership;
            Mutex singleInstanceMutex = null;
            try
            {
                singleInstanceMutex = new Mutex(true, mutexName, out grantedOwnership);
                if (!grantedOwnership)
                {
                    Environment.Exit(0);
                    return;
                }
            }
            finally
            {
                singleInstanceMutex.Close();
            }
            string mainArgs= (0<(Args?.Length??0))? string.Join(" ",Args):"";
            AutoUpdateLog.LogObj.AddLog(LOG_MODE.INFO, $"mainArgs is: {mainArgs}");
            Common.CheckDir();
            //是否从程序中调用
            bool callFromMainProg = false;
            List<string> actualMainArgs = new List<string>();
            if (null != Args)
            {
                foreach (string arg in Args)
                {
                    if (arg.Equals("/CALLFROMPROG", StringComparison.OrdinalIgnoreCase)
                        || arg.Equals("--CALLFROMPROG", StringComparison.OrdinalIgnoreCase))
                        callFromMainProg = true;
                    else
                    {
                        if(!string.IsNullOrWhiteSpace(arg))
                            actualMainArgs.Add(arg);
                    }
                }
            }
            Common.CallFromMainProg = callFromMainProg;
            Common.MainProgArgs = actualMainArgs.ToArray();
#if CONSOLE_MODE
            new UpdateEntry().StartUpdate();
#else
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FrmUpdate());
#endif
        }
    }
}
