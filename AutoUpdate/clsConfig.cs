﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Text;
namespace BIN
{
    enum CONFIG_KEY
    {
        UPDATE_KEY, //升级关键字,对应limit.xml的LimitKey
        IS_UPDATE_AGENT, //是否自动升级代理
        AGENT_UPDATE_INTERVAL ,//处动升级代理的升级间隔时间(M)
        SHARE_FOLDER, //自动升级代理的共享目录
        FIXXED_UPDATE_URL, //固定的升级地址(UpdateSilent)
        ONLY_KEEP_ME,  //升级完之后只保留<Url>
        USE_BAT
    }
    /**********************
     * author :bin
     * 保存配置文件
     * ********************/
    class clsConfig
    {
        private string _ConfigFile = string.Empty;
        public clsConfig()
        {
            _ConfigFile = Common.CombineDir(Common.GetAppDir(), "UserConfig.xml");
            if (!Common.FileExists(_ConfigFile))
            {
                XmlFiles xmlFile = new XmlFiles(_ConfigFile);
                XmlNode pNode = xmlFile.SelectSingleNode("//");
                if (null != pNode)
                {
                    pNode.AppendChild( xmlFile.CreateElement("Config"));
                }
                xmlFile.Save();
            }
        }
        public string this[string SectionKey]
        {
            get
            {
                return GetSectionValue(SectionKey);
            }
            set
            {
                SetSectionValue(SectionKey, value);
            }
        }
        public string this[CONFIG_KEY confKey]
        {
            get
            {
                return GetSectionValue(confKey.ToString());
            }
            set
            {
                SetSectionValue(confKey.ToString(), value);
            }
        }
        private string GetSectionValue(string SectionKey)
        {
            string xpath = string.Format("//Config//{0}", SectionKey);
            return XPathGet.GetValue(_ConfigFile, xpath, string.Empty);
        }
        public string GetConfigValue(string sectionKey, string attribute)
        {
            string xpath = string.Format("//Config//{0}", sectionKey);
            return XPathGet.GetValue(_ConfigFile, xpath, attribute);
        }
        public XmlNode GetXmlNode(string sectionKey)
        {
            string xpath = string.Format("//Config//{0}", sectionKey);
            return XPathGet.GetNode(_ConfigFile, xpath);
        }
        private void SetSectionValue(string SectionKey, string Value)
        {
            XmlFiles xmlFile = new XmlFiles(_ConfigFile);
            string xpath = string.Format("//Config//{0}", SectionKey);
            var pNode = xmlFile.SelectSingleNode(xpath);
            if (null == pNode)
            {
                pNode = xmlFile.CreateElement(SectionKey);
                xmlFile.SelectSingleNode("//Config").AppendChild(pNode);
            }
            pNode.InnerText = Value;
            xmlFile.Save();
        }
    }
}
