﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;
namespace BIN
{
    public partial class FrmUpdate : Form
    {
        private int _copiedFileCount = 0;
        //以下四个变量由GetMainProgInfo获取
        protected string _mainProgShortName = "", _mainProgFullName = "";
        protected int _mainProgPId = 0;
        protected bool _isMainProgKilled = false;

        private string updateUrl = string.Empty;
        private string tempUpdatePath = string.Empty;//临时更新文件的目录
        private string tempDir = string.Empty; // temp目录
        private  XmlFiles updaterXmlFiles = null;//本地配置文件对象
        private int availableUpdate = 0; //可更新的文件
        private int fileCountDownloaded = 0; //已经下载的更新文件
        private bool _AllKeyUptWasOK= false;
        private string localXmlFile = string.Empty;
        private string serverXmlFile = string.Empty;
        private AppUpdater appUpdater = null;
        private string AppDir = string.Empty;
        //以下是三种更新模式
        private bool _IsCopyFolderMode = false;
        private bool _IsDelayUpdateMode = false;
        private bool _IsSilentMode = false;
        private NotifyIcon _ntyIcon = null;
        private event EventHandler OnFileDownloaded;
        private string _restartAlert = string.Empty;
        private DateTime? _startUpdateTime = null;
        private const int RESTART_WAIT_SECOND = 30; //等待多少秒就自动杀主进程
        private const int CONFIRM_UPTTIME_BIG = 5; //更新少于多少秒就不采用CONFIRM模式
        private AutoUpdateLog _logger = null;
        private bool _ExedStopBat = false;
        public string ShortConfigName { get; set; } = "UpdateList.xml";
        /// <summary>
        /// 下载了一个文件
        /// </summary>
        private void DownLoaded1File()
        {
            ++fileCountDownloaded;
            if (null != OnFileDownloaded) OnFileDownloaded(this, null);
        }
        /// <summary>
        /// 是否是静默模式的更新
        /// </summary>
        /// <returns></returns>
        private bool IsSilentMode(XmlFiles localXmlFile)
        {
            bool Ret = Common.CallFromMainProg && AppUpdater.IsSilentMode(localXmlFile);
            _logger.AddLog(LOG_MODE.INFO,$"Is silent Updating ? {Ret}");
            return Ret;
        }
        /// <summary>
        /// 是否是复制目录的更新方式
        /// </summary>
        private bool IsCopyFolderUpdating()
        {
            bool Ret= Common.CallFromMainProg 
                && AppUpdater.RewakeUpdateMode 
                && Common.FileExists(serverXmlFile)
                && System.IO.Directory.Exists(tempUpdatePath) ;
            _logger.AddLog(LOG_MODE.INFO,$"Is Copy Folder Updating ? {Ret}");
            return Ret;
        }
        private bool IsDelayUpdateMode()
        {
            bool Ret = Common.CallFromMainProg
                  && AppUpdater.RewakeUpdateMode ;
            _logger.AddLog(LOG_MODE.INFO,$"Is Delay Updating ? {Ret}" );
            return Ret;
        }
        public FrmUpdate()
        {
            InitializeComponent();
            _logger = BIN.AutoUpdateLog.LogObj;
            FrmUpdate.CheckForIllegalCrossThreadCalls = false;
            _startUpdateTime = DateTime.Now;
            AppDir = AppDomain.CurrentDomain.BaseDirectory;
            tempDir = Common.CombineDir(AppDir, "temp");
            tempUpdatePath = Common.CombineDir(tempDir, "Downloads\\");
            //服务端配置文件下载到本地
            serverXmlFile = Common.CombineDir(tempDir, this.ShortConfigName);
            //取本地配置文件
            localXmlFile = Common.CombineDir(AppDir, this.ShortConfigName);
            _logger.AddLog(LOG_MODE.INFO, $"Use config file '{localXmlFile}' ,start updating...");
            if (!(System.IO.File.Exists(localXmlFile))) //不存在配置文件
            {
                _logger.AddLog(LOG_MODE.ERROR, $"File '{localXmlFile}' doesn't exist ,You can use (*.EXE) --NOUPDATE to start the program!");
                ExitUpdateProg();
                return;
            }
            //从本地读取更新配置文件信息
            updaterXmlFiles = new XmlFiles(localXmlFile);
            _IsSilentMode = IsSilentMode(updaterXmlFiles);
            if (!_IsSilentMode)
            {
                //是否是目录复制模式
                _IsCopyFolderMode = IsCopyFolderUpdating();
                //是否是延迟更新模式
                if (!_IsCopyFolderMode)
                {
                    _IsDelayUpdateMode = IsDelayUpdateMode();
                }
            }
            else
            {
                this.OnFileDownloaded += new EventHandler(FrmUpdate_OnFileDownloaded);
            }
            GetMainProgInfo();
            if (string.IsNullOrEmpty(_mainProgShortName))
            {
                string info="Config file error,Please check the xml node 'EntryPoint'!";
                _logger.AddLog(LOG_MODE.WARN,info );
                throw new Exception(info);
            }
            _restartAlert = $"{_mainProgShortName} will restart in {RESTART_WAIT_SECOND} seconds";
            _logger.AddLog(LOG_MODE.INFO, $"Main prog was runned ? {_mainProgPId > 0} ");
            try
            {
                if (!_IsCopyFolderMode)
                {
                    Init();
                }
            }
            catch (Exception ex)
            {
                 _logger.AddLog(LOG_MODE.ERROR,$"{ex.Message},{ex.Source},{ex.StackTrace}");
                ExitUpdateProg();
            }
        }
        private void GetMainProgInfo()
        {
            _mainProgShortName = updaterXmlFiles.GetNodeValue("//EntryPoint");
            _logger.AddLog(LOG_MODE.INFO, $"Main prog is :{_mainProgShortName}!");
            _mainProgFullName = Common.CombineDir(AppDir, _mainProgShortName);
            _mainProgPId = Common.ProgramRunned(_mainProgFullName, true, false);
        }
        /// <summary>
        /// 成功下载了文件时的事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void FrmUpdate_OnFileDownloaded(object sender, EventArgs e)
        {
            if (null != _ntyIcon && _IsSilentMode)
            {
                _ntyIcon.Text = $"Downloading {fileCountDownloaded} / {availableUpdate}" ;
            }
        }
        private string GetFileFix(string fixPath, string FullPath)
        {
            int index = FullPath.IndexOf(fixPath);
            if (index < 0) return FullPath;
            return FullPath.Substring(index+fixPath.Length , FullPath.Length - fixPath.Length );
        }
        private void Init()
        {
            btnFinish.Visible = false;
            string dir = System.IO.Path.Combine(AppDir, "Logs");
            if (!System.IO.Directory.Exists(dir)) 
                System.IO.Directory.CreateDirectory(dir);
            try
            {
                appUpdater = new AppUpdater(updaterXmlFiles);
                appUpdater.ShortConfigName = this.ShortConfigName;
                appUpdater.AppDir = AppDir;
                if (!appUpdater.NeedUpdate)
                {
                    _logger.AddLog(LOG_MODE.INFO, "No update necessary(configged),exit.");
                    ExitUpdateProg();
                    return;
                }
                //获取服务器地址
                bool isAutoupdateAgent = Common.IsUpdateAgent;
                _logger.AddLog(LOG_MODE.INFO, $"IsUpdateAgent?{isAutoupdateAgent}");
                if (isAutoupdateAgent)
                {
                    updateUrl = Common.Config[CONFIG_KEY.FIXXED_UPDATE_URL];
                    if (string.IsNullOrWhiteSpace(updateUrl))
                    {
                        updateUrl = appUpdater.TestGetUpdateUrl();
                    }
                    else
                    {
                        appUpdater.FillLimitKey(AppUpdater.DownloadTempDir);
                    }
                }
                else
                {
                    updateUrl = appUpdater.TestGetUpdateUrl();
                }
                if (string.IsNullOrEmpty( updateUrl))
                {
                    _logger.AddLog(LOG_MODE.WARN, "Config file error!,Please check xml node 'Url'.");
                    ExitUpdateProg();
                    return;
                }
                else
                {
                    _logger.AddLog(LOG_MODE.INFO, $"Update from url '{updateUrl}'!");
                }
                //这里就已经取到了服务端设置的LimitKey
                try
                {
                    _logger.AddLog(LOG_MODE.INFO,$"LimitKey is :{appUpdater.LimitKey},Local UpdateKey is:{Common.UpdateKey}");
                }
                catch (Exception ex)
                {
                    appUpdater.WriteException(ex);
                }
                if (appUpdater.CurrPointWasLimited())
                {
                    _logger.AddLog(LOG_MODE.WARN, "AutoUpdate was not allowwed!");
                    ExitUpdateProg();
                }
                appUpdater.UpdateBaseUrl = updateUrl;
                appUpdater.UpdateListUrl = Common.CombileUrl(updateUrl, this.ShortConfigName);
                //下载服务端的配置文件
                if (!Common.UpdateListFileIsOk(Common.CombineDir(tempUpdatePath, this.ShortConfigName)))
                {
                    appUpdater.DownAutoUpdateFile(tempDir);
                }
            }
            catch
            {
                _logger.AddLog(LOG_MODE.ERROR, $"Syntax error.({appUpdater.UpdateListUrl})");
                ExitUpdateProg();
                return;
            }
            //获取更新文件列表
            Hashtable htUpdateFile = new Hashtable();
            try
            {
                if (!File.Exists(serverXmlFile))
                {
                    _logger.AddLog(LOG_MODE.ERROR, $"Error occurs when saving server config file {serverXmlFile} to local!");
                    ExitUpdateProg();
                    return;
                }
                availableUpdate = appUpdater.CheckForUpdate(serverXmlFile, localXmlFile, out htUpdateFile);
                _logger.AddLog(LOG_MODE.INFO,$"available update file count: {availableUpdate}!");
                if (availableUpdate > 0)
                {
                    for (int i = 0; i < htUpdateFile.Count; i++)
                    {
                        string[] fileArray = (string[])htUpdateFile[i];
                        lvUpdateList.Items.Add(new ListViewItem(fileArray));
                    }
                }
                else
                {
                    _logger.AddLog(LOG_MODE.INFO, "No updating necessary!");
                    if (null != appUpdater)
                    {
                        if (appUpdater.ServerConfigIsNewer())
                        {
                            File.Copy(serverXmlFile, localXmlFile, true);
                        }
                    }
                    ExitUpdateProg();
                    return;
                }

            }
            catch(Exception ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                ExitUpdateProg();
                return;
            }
        }
        private void FrmUpdate_Load(object sender, System.EventArgs e)
        {
            this.Text = _mainProgShortName;
            lblCaption.Text = $"{_mainProgShortName} AutoUpdate";
            if (_IsSilentMode)//静默模式不显示窗体
            {
                this.ShowInTaskbar = false;
                this.Hide();
                _logger.AddLog(LOG_MODE.INFO, "It's silent mode , hide self!");
                try
                {
                    if (null != this.components)
                    {
                        _ntyIcon = new NotifyIcon(this.components);
                    }
                    else
                    {
                        _ntyIcon = new NotifyIcon();
                    }
                    _ntyIcon.Visible = true;
                    _ntyIcon.Icon = Properties.Resources.down;
                    _ntyIcon.BalloonTipTitle = _mainProgShortName;
                    _ntyIcon.MouseDoubleClick += new MouseEventHandler(_ntyIcon_MouseDoubleClick);
                }
                catch { }
            }
            if (!_IsCopyFolderMode)
            {
                if (availableUpdate > 0)
                {
                    this.Invoke(new Action(delegate
                    {
                        btnNext.Visible = true;
                        btnNext.Enabled = true;
                        doNext();
                    }));
                }
            }
            else//目录复制模式直接从临时文件中复制
            {
                this.Invoke(new Action(delegate
                {
                    doFinish();
                 }));
            }
        }

        void _ntyIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (!this.Visible)
            {
                this.Show();
                this.ShowInTaskbar = true;
            }
            else
            {
                this.ShowInTaskbar = false;
                this.Hide();
            }
        }
        /// <summary>
        /// 延迟更新模式不能删除文件_IsDelayUpdateMode=true
        /// </summary>
        private void DelTempDir()
        { 
            try
            {
                if (System.IO.Directory.Exists(tempUpdatePath) && !_IsDelayUpdateMode)
                {
                    _logger.AddLog(LOG_MODE.INFO,$"Delete directory:{tempUpdatePath}");
                    System.IO.Directory.Delete(tempUpdatePath, true);
                }
                else
                {
                    _logger.AddLog(LOG_MODE.INFO,$"No necessary to delete directory:{tempUpdatePath},It's empty or delay updating mode!");
                }
            }
            catch { }
            try
            {
                if (Common.FileExists(serverXmlFile) && ( !_IsDelayUpdateMode || 0==availableUpdate ))
                {
                    _logger.AddLog(LOG_MODE.INFO,$"Delete configfile:{serverXmlFile}");
                    System.IO.File.Delete(serverXmlFile);
                }
                else
                {
                    _logger.AddLog(LOG_MODE.INFO,$"No necessary to delete file:{serverXmlFile},It doesn't exists or delay updating mode!");
                }
            }
            catch { }
        }
        private void ExitUpdateProg()
        {
            DelTempDir();
            _logger.AddLog(LOG_MODE.INFO, $"Is main prog killed ? {_isMainProgKilled}");
            if (_isMainProgKilled || Common.CallFromMainProg || _IsSilentMode)
            {
                StartMainProgNoUpdate();
            }
            if (appUpdater.UseBat && appUpdater.AlwaysExecStartBat.IsTrue())
            {
                ExecStartBat();
            }
            _logger.AddLog(LOG_MODE.INFO, "exit normal! ");
            Environment.Exit(0);
        }
        private void btnCancel_Click(object sender, System.EventArgs e)
        {
            ExitUpdateProg();
        }
        private void btnNext_Click(object sender, System.EventArgs e)
        {
            doNext();
        }
        private void doNext()
        { 
            btnNext.Visible = false;
            if (availableUpdate > 0)
            {
                Thread threadDown = new Thread(new ThreadStart(DownUpdateFile));
                threadDown.IsBackground = true;
                threadDown.Start();
            }
            else
            {
                _logger.AddLog(LOG_MODE.INFO, "No updating necessary!");
                ExitUpdateProg();
                return;
            }
        }
        private string GetMainProgExe()
        {
            string ret = updaterXmlFiles.GetNodeValue("//EntryPoint");
            _logger.AddLog(LOG_MODE.INFO, $"Main prog is :{ret}!");
          return ret;
        }
        private void StartMainProgNoUpdate()
        {
            try
            {
                if (!_isMainProgKilled) return;
                _logger.AddLog(LOG_MODE.INFO, $"Start main prog {_mainProgFullName} --NOUPDATE ");
                Common.BeginProcess(_mainProgFullName, Common.GetMainProgArgsNoUpdate(), AppDir);
            }
            catch (Exception ex)
            {
                if (!_IsSilentMode)
                {
                    Console.WriteLine($"StartMainProgNoUpdate,{ex.Message}");
                }
            }
        }
        private void StartSpecifiedProgNoUpdate(string ShortProgName)
        {
            try
            {
                if (!_isMainProgKilled) return;
                string Cmd = Common.CombineDir(AppDir, ShortProgName);
                _logger?.AddLog(LOG_MODE.INFO, $"Start prog {Cmd} --NOUPDATE ");
                Common.BeginProcess(Cmd, Common.GetMainProgArgsNoUpdate(), AppDir);
            }
            catch (Exception ex)
            {
                if (!_IsSilentMode)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }
        private void KillMainProg()
        {
            if (appUpdater.UseBat) //使用bat不能直接杀进程
            {
                _logger?.AddLog(LOG_MODE.INFO, $"KillMainProg?No,Because it's UseBat.");
                return;
            }
            if (0 < _mainProgPId)
            {
                _logger.AddLog(LOG_MODE.INFO, $"Main prog is running ,try to kill...");
                _isMainProgKilled = Common.IsProgExists(_mainProgPId, true);
            }
            if (0 < (appUpdater.KillProgams?.Length ?? 0))
            {
                foreach (string prog in appUpdater.KillProgams)
                {
                    if (prog.Equals(_mainProgShortName, StringComparison.OrdinalIgnoreCase)) continue;
                    _logger.AddLog(LOG_MODE.INFO, $"Kill program '{prog}'...");
                    try
                    {
                        string fullProgName = Common.CombineDir(AppDir, prog);
                        Common.ProgramRunned(fullProgName, true, true);
                        _logger.AddLog(LOG_MODE.INFO, "            OK!");
                    }
                    catch (Exception ex)
                    {
                        _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                    }
                }
                Thread.Sleep(500);
            }
            if (_isMainProgKilled) Thread.Sleep(500);
            _logger.AddLog(LOG_MODE.INFO, $"Main prog is killed :{_isMainProgKilled}!");
        }
        private void DownUpdateFile()
        { 
            if  (AppUpdater.PartialDowning)
                DownUpdateFilePart();
            else
                DownUpdateFileWhole();
        }
        private void DownUpdateFilePart()
        {
            string user = "", pass = "";
            string actUrl = updateUrl;
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lv = null;
            string KeyUptDesc = string.Empty;
            bool isFtp = updateUrl.IsFtp();
            if (isFtp)
            {
                actUrl = Common.SplitFtpUrl(updateUrl, out user, out pass);
            }
            try
            {
                for (int i = 0; i < this.lvUpdateList.Items.Count; i++)
                {
                    string UpdateFile = lvUpdateList.Items[i].Text.Trim();
                    string updateFileUrl = Common.CombileUrl(actUrl, UpdateFile);
                    string tempPath  = Common.CombineDir(tempUpdatePath, UpdateFile);
                    WebRequest webReq = null;
                    WebResponse webRes = null;
                    Common.DealFileDirect(ref tempPath);
                    CreateDirtory(tempPath);
                    lv = lvUpdateList.Items[i];
                    KeyUptDesc = lv.SubItems[3].Text;
                    long fileLength = 0;
                    Stream downStream = null;
                    try
                    {
                        if (!isFtp)
                        {
                            webReq = WebRequest.Create(updateFileUrl);
                            webReq.UseDefaultCredentials = true;
                        }
                        else
                        {
                            webReq = (FtpWebRequest)WebRequest.Create(updateFileUrl);
                            webReq.Method = WebRequestMethods.Ftp.DownloadFile;
                            webReq.Credentials = new NetworkCredential(user, pass);
                            ((FtpWebRequest)webReq).UseBinary = true;
                        }
                        webReq.Timeout = appUpdater.DownloadingTimeout;
                        webRes = webReq.GetResponse();
                        lbState.Text = "Downloading the update file, please wait...";
                        fileLength = webRes.ContentLength; //ftp取不到值
                        downStream = webRes.GetResponseStream();
                        pbDownFile.Value = 0;
                        if(!isFtp)
                            pbDownFile.Maximum = (int)fileLength;
                        else
                             pbDownFile.Maximum = 100;
                        if (-1 < fileLength)
                        {
                            StreamReader srmReader = null;
                            FileStream fs = null;
                            try
                            {
                                srmReader = new StreamReader(downStream);
                                byte[] bufferbyte = new byte[fileLength];
                                int allByte = (int)bufferbyte.Length;
                                int startByte = 0;
                                while (fileLength > 0)
                                {
                                    Application.DoEvents();
                                    int downByte = downStream.Read(bufferbyte, startByte, allByte);
                                    if (downByte == 0) break;
                                    startByte += downByte;
                                    allByte -= downByte;
                                    pbDownFile.Value += downByte;
                                    float part = (float)startByte / 1024;
                                    float total = (float)bufferbyte.Length / 1024;
                                    int percent = Convert.ToInt32((part / total) * 100);
                                    this.lvUpdateList.Items[i].SubItems[2].Text = $"{percent}%";
                                }
                                fs = new FileStream(tempPath, FileMode.OpenOrCreate, FileAccess.Write);
                                fs.Write(bufferbyte, 0, bufferbyte.Length);
                            }
                            finally
                            {
                                srmReader?.Close();
                                fs?.Close();
                                fs?.Dispose();
                                srmReader?.Dispose();
                            }
                        }
                        else
                        {
                            downStream.StreamToFile(tempPath, 8192, true, writeBytes =>
                            {
                                decimal downKB = Math.Round(writeBytes / 1024.0m);
                                this.lvUpdateList.Items[i].SubItems[2].Text = $"{downKB}K";
                                pbDownFile.Value += 1;
                            });
                        }
                        _logger.AddLog(LOG_MODE.INFO, $"downloading to {Environment.NewLine}    {tempPath} ... ,OK!");
                        if (appUpdater?.CheckMd5 ?? false)
                        {
                            string md5 = appUpdater.GetMd5(UpdateFile);
                            if (!string.IsNullOrEmpty(md5) && Common.FileExists(tempPath))
                            {
                                string md5Act = appUpdater.GetMD5HashFromFile(tempPath);
                                if (!md5.Equals(md5Act, StringComparison.OrdinalIgnoreCase))
                                {
                                    AutoUpdateLog.LogObj.AddLog(LOG_MODE.ERROR, $"File: {UpdateFile} MD5 ERROR !");
                                    ExitUpdateProg();
                                    if (_isMainProgKilled) StartMainProgNoUpdate();
                                    return;
                                }
                            }
                        }
                        DownLoaded1File();
                    }
                    catch (Exception ex) //捕获一般性错误，如果是关键更新错误，则再抛出
                    {
                        _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                        string ErrorText = string.Empty;
                        if (KeyUptDesc.IsTrue()) //关键更新错误
                        {
                            _AllKeyUptWasOK = false;
                            ErrorText = $"Error occurs when downloading key file'{UpdateFile}'(Failure updating) ,exit";
                            _logger.AddLog(LOG_MODE.ERROR, ErrorText);
                            ExitUpdateProg();
                            if (_isMainProgKilled) StartMainProgNoUpdate();
                            return;
                        }
                        else
                        {
                            this.lvUpdateList.Items[i].SubItems[2].Text = "FAIL";
                            ErrorText = $"Error occurs when downloading non key file '{UpdateFile}',(ignore),continue...";
                            _logger.AddLog(LOG_MODE.WARN, ErrorText);
                            continue;
                        }
                    }
                    finally
                    {
                        webRes?.Close();
                        downStream?.Close();
                        downStream?.Dispose();
                    }
                }
                lbState.Text = "All files downloaded completely!";
                _AllKeyUptWasOK = true;
                if (_AllKeyUptWasOK)
                {
                    if (AppUpdater.AutoFinish)
                    {
                        this.Invoke(new Action(delegate
                        {
                            doFinish();
                        }));
                    }
                }
                InvalidateControl();
            }
            catch (WebException ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                btnCancel.Visible = true;
                btnCancel.Enabled = true;
                btnFinish.Visible = false;
                btnNext.Visible = false;
                ExitUpdateProg();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void DownUpdateFileWhole()
        {
            WebClient wcClient = null;
            this.Cursor = Cursors.WaitCursor;
            ListViewItem lv = null;
            string KeyUptDesc = string.Empty;
            try
            {
                int FileCount = this.lvUpdateList.Items.Count;
                pbDownFile.Maximum = FileCount;
                pbDownFile.Value = 0;
                wcClient = new WebClient();
                if (!updateUrl.IsFtp())
                {
                    wcClient.UseDefaultCredentials = true;
                }
                for (int i = 0; i < FileCount; i++)
                {
                    string UpdateFile = lvUpdateList.Items[i].Text.Trim();
                    string tempPath = Common.CombineDir( tempUpdatePath , UpdateFile);
                    Common.DealFileDirect(ref tempPath);
                    string updateFileUrl = Common.CombileUrl( updateUrl , UpdateFile);
                    lv = lvUpdateList.Items[i];
                    KeyUptDesc = lv.SubItems[3].Text;
                    try
                    {
                        CreateDirtory(tempPath);
                        wcClient.DownloadFile(updateFileUrl, tempPath);
                        pbDownFile.Value++;
                        this.lvUpdateList.Items[i].SubItems[2].Text = "OK!";
                        _logger.AddLog(LOG_MODE.INFO, $"Download to {tempPath} ... ,OK!");
                    }
                    catch (Exception ex) //捕获一般性错误，如果是关键更新错误，则再抛出
                    {
                        _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                        string ErrorText = string.Empty;
                        if (KeyUptDesc.IsTrue()) //关键更新错误
                        {
                            _AllKeyUptWasOK = false;
                            ErrorText = $"Error occurs when downloading key file'{UpdateFile}'(Failure updating) ,exit";
                            _logger.AddLog(LOG_MODE.ERROR, ErrorText);
                            ExitUpdateProg();
                            return;
                        }
                        else
                        {
                            this.lvUpdateList.Items[i].SubItems[2].Text = "Fail";
                            ErrorText = $"Error occurs when downloading non key file '{UpdateFile}',(ignore),continue...";
                            _logger.AddLog(LOG_MODE.WARN, ErrorText);
                            continue;
                        }
                    }
                }
                _AllKeyUptWasOK = true;
                if (_AllKeyUptWasOK)
                {
                    if (AppUpdater.AutoFinish)
                    {
                        this.Invoke(new Action(delegate
                        {
                            doFinish();
                        }));
                    }
                }
                InvalidateControl();
            }
            catch (WebException ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                btnCancel.Visible = true;
                btnCancel.Enabled = true;
                btnFinish.Visible = false;
                btnNext.Visible = false;
                ExitUpdateProg();
            }
            finally
            {
                this.Cursor = Cursors.Default;
                if (null != wcClient)
                {
                    wcClient.Dispose();
                }
            }
        }
        //创建目录
        private void CreateDirtory(string path)
        {
            if (!File.Exists(path))
            {
                string[] dirArray = path.Split('\\');
                string temp = string.Empty;
                for (int i = 0; i < dirArray.Length - 1; i++)
                {
                    temp += dirArray[i].Trim() + "\\";
                    if (!Directory.Exists(temp))
                    {
                        Directory.CreateDirectory(temp);
                        _logger.AddLog(LOG_MODE.INFO, $"Create folder {temp} ... ,OK!");
                    }
                }
            }
        }
        public void CopyFile(string sourcePath, string objPath)
        {
            string oriFile = string.Empty, renTo = string.Empty, fileFix = string.Empty;
            string txt = string.Empty;
            if (!Directory.Exists(objPath))
            {
                Directory.CreateDirectory(objPath);
                _logger.AddLog(LOG_MODE.INFO, $"Create folder {objPath}... ,OK!");
            }
            string[] files = Directory.GetFiles(sourcePath);
            string tmpfile = string.Empty;
            for (int i = 0; i < files.Length; i++)
            {
                string[] childfile = files[i].Split('\\');
                oriFile = childfile[childfile.Length - 1];
                if (oriFile.EndsWith(".tmp")) continue;
                tmpfile = oriFile;
                if (tmpfile.Equals("AUTOUPDATE.EXE", StringComparison.OrdinalIgnoreCase)
                    || tmpfile.Equals("USERCONFIG.XML", StringComparison.OrdinalIgnoreCase)) continue;
                if (!_IsDelayUpdateMode)
                {
                    fileFix = GetFileFix(tempUpdatePath, files[i]);
                    renTo = appUpdater.FileRenameTo(fileFix);
                    childfile = renTo.Split('\\');
                    renTo = childfile[childfile.Length - 1];
                }
                txt = Common.CombineDir(objPath, oriFile);
                File.Copy(files[i],txt  , true); //原文件名也复制一次
                ++_copiedFileCount;
                if ( !string.IsNullOrEmpty(renTo) && !_IsDelayUpdateMode )
                {
                    txt = Common.CombineDir( objPath , renTo );
                    File.Copy(files[i], txt, true);
                    _logger.AddLog(LOG_MODE.INFO,$"Execute rename operating,'{files[i]}'==>'{txt}'!");
                    ++_copiedFileCount;
                }
                _logger.AddLog(LOG_MODE.INFO, $"Copy file{Environment.NewLine}      {files[i]} -->{Environment.NewLine}       {txt}");
            }
            string[] dirs = Directory.GetDirectories(sourcePath);
            for (int i = 0; i < dirs.Length; i++)
            {
                string[] childdir = dirs[i].Split('\\');
                txt= Common.CombineDir( objPath, childdir[childdir.Length - 1]) ;
                CopyFile(dirs[i],txt );
                _logger.AddLog(LOG_MODE.INFO, $"Copy File{Environment.NewLine}      {dirs[i]} -->{Environment.NewLine}       {txt}" );
            }
        }
        //点击完成复制更新文件到应用程序目录
        private void btnFinish_Click(object sender, System.EventArgs e)
        {
            doFinish();
        }
        private void doFinish()
        {
            DateTime curr = DateTime.Now;
            int updateSeconds= (int)(Math.Abs( Common.DateDiff(DATEPART.SECOND,curr,_startUpdateTime.Value)));
            _logger.AddLog(LOG_MODE.INFO,$"It used {updateSeconds} seconds to download the files!");
            if(updateSeconds<=CONFIRM_UPTTIME_BIG)
            {
              _logger.AddLog(LOG_MODE.INFO,$"updating time less then {CONFIRM_UPTTIME_BIG} seconds,no confirm necessary!");
            }
            if (_IsSilentMode && updateSeconds>CONFIRM_UPTTIME_BIG)
            {
                doFinishConfirm();
            }
            else
            {
                doFinishNormal();
            }
        }
        private int _waitedSeconds = 0;
        private bool _shifted = false;
        private void calc()
        {
            while (_waitedSeconds < RESTART_WAIT_SECOND)
            {
                System.Threading.Thread.Sleep(1000);
                ++_waitedSeconds;
                if (_waitedSeconds > 5 && !_shifted)
                {
                    _restartAlert = $"Restart {_mainProgShortName} immediately!";
                }
                this.Invoke(new Action(delegate
                {
                    btnRestartMain.Text =$"{_restartAlert} {RESTART_WAIT_SECOND - _waitedSeconds}";
                }));
            }
        }
        private void doFinishConfirm()
        {
            this.ShowInTaskbar = true;
            this.Show();
            Application.DoEvents();
            btnCancel.Visible = false;
            btnFinish.Visible = false;
            btnNext.Visible = false;
            btnRestartMain.Visible = true;
            this.TopMost = true;
            new MyThreadPool(this, calc, doFinishNormal, true);
        }
        private void doFinishNormal()
        {
            try
            {
                if (!_IsDelayUpdateMode)
                {
                    if (!_AllKeyUptWasOK && !_IsCopyFolderMode && !_IsDelayUpdateMode)
                    {
                        string info = "Error occurs when updating key file,Update failed!";
                        _logger.AddLog(LOG_MODE.INFO, info);
                        throw new Exception(info);
                    }
                    else
                    {
                        if (appUpdater.UseBat
                            && !Common.CallFromMainProg
                            && !string.IsNullOrWhiteSpace(appUpdater.StopBat))
                        {
                            ExecStopBat();
                        }
                        //延迟模式不杀主进程
                        if (0<_mainProgPId && !appUpdater.UseBat)
                        {
                            _logger.AddLog(LOG_MODE.INFO, "Main prog was runned ,try to kill...");
                            KillMainProg();
                            _logger.AddLog(LOG_MODE.INFO,"Kill finished!");
                            Thread.Sleep(100);
                        }
                        try
                        {
                            _logger.AddLog(LOG_MODE.INFO,$"Begin copy folder...{tempUpdatePath}=>{AppDir}");
                            CopyFile(tempUpdatePath, AppDir);
                            if (0 < _copiedFileCount)
                            {
                                if (Common.FileExists(serverXmlFile))
                                {
                                    _logger.AddLog(LOG_MODE.INFO,$"Begin copy config file...{serverXmlFile}=>{localXmlFile}");
                                    System.IO.FileInfo fi = new System.IO.FileInfo(serverXmlFile);
                                    if (fi.Exists && fi.Length > 1.0)
                                    {
                                        fi.CopyTo(localXmlFile, true);
                                        fi = null;
                                    }
                                }
                            }
                            Thread.Sleep(200);
                            if (!Common.CallFromMainProg
                                && !string.IsNullOrWhiteSpace(appUpdater.StartBat)
                                && !appUpdater.AlwaysExecStartBat.IsTrue() //如果总是执行启用脚本就移到退出程序中
                                && appUpdater.UseBat)
                            {
                                ExecStartBat();
                            }
                        }
                        finally
                        {
                        }
                        if (Common.IsUpdateAgent)
                        {
                            if (!string.IsNullOrEmpty(Common.ShareFolder))
                            {
                                XmlFiles obj = new XmlFiles(localXmlFile);
                                System.Xml.XmlNodeList lst = obj.GetNodeList("//Url");
                                if (null != lst && lst.Count > 0)
                                {
                                    lst[0].Value = Common.ShareFolder;
                                    obj.Save();
                                }
                                obj = null;
                            }
                        }
                        if (!_IsDelayUpdateMode)
                        {
                            System.IO.Directory.Delete(tempUpdatePath, true);
                        }
                        _logger.AddLog(LOG_MODE.INFO, "Update success!");

                    }
                }
                else
                {
                    _logger.AddLog(LOG_MODE.INFO, "It's deley updating mode,exit autoupdate directly!");
                }
            }
            catch (Exception ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, $"Update failed!Error:{ex.Message}");
            }
            finally
            {
                ExitUpdateProg();
            }
        }
        //重新绘制窗体部分控件属性
        private void InvalidateControl()
        {
            this.Invoke(new Action(delegate
            {
                btnNext.Visible = false;
                btnCancel.Visible = false;
                btnFinish.Location = btnCancel.Location;
                btnFinish.Visible = true;
            } ));
        }
        private void ExecStopBat()
        {
            string fileName = Common.CombineDir(AppDir, appUpdater.StopBat);
            if (!File.Exists(fileName))
            {
                try
                {
                    Common.Download(appUpdater.UpdateBaseUrl, "stop.txt", AppDir);
                    string tmpfileName = Common.CombileUrl(AppDir, "stop.txt");
                    if (Common.FileExists(tmpfileName))
                    {
                        FileInfo fi = new FileInfo(tmpfileName);
                        fi.CopyTo(fileName);
                        fi.Delete();
                    }
                }
                catch (Exception ex)
                {
                    _logger?.AddLog(LOG_MODE.ERROR, $"Down stop.txt,{ex.Message}");
                }
            }
            if (File.Exists(fileName))
            {
                _logger.AddLog(LOG_MODE.INFO, $"BeginProcess stop bat: {fileName}");
                try
                {
                    Common.BeginProcess(fileName, AppDir, hiddenWindow: true, waitforExit: true);
                    _ExedStopBat = true;
                }
                catch (Exception ex)
                {
                    _logger.AddLog(LOG_MODE.ERROR, $"BeginProcess,{ex.Message}");
                }
                finally
                {
                    _logger.AddLog(LOG_MODE.INFO, $"StopBat finished.");
                }
            }
        }
        private void ExecStartBat()
        {
            string fileName = Common.CombineDir(AppDir, appUpdater.StartBat);
            if (!File.Exists(fileName))
            {
                try
                {
                    Common.Download(appUpdater.UpdateBaseUrl, "start.txt", AppDir);
                    string tmpfileName = Common.CombileUrl(AppDir, "start.txt");
                    if (Common.FileExists(tmpfileName))
                    {
                        FileInfo fi = new FileInfo(tmpfileName);
                        fi.CopyTo(fileName);
                        fi.Delete();
                    }
                }
                catch (Exception ex)
                {
                    _logger?.AddLog(LOG_MODE.ERROR, $"Down start.txt,{ex.Message}");
                }
            }
            if (File.Exists(fileName))
            {
                _logger.AddLog(LOG_MODE.INFO, $"BeginProcess start bat： {fileName}");
                try
                {
                    Common.BeginProcess(fileName, AppDir, hiddenWindow: true, waitforExit: true);
                }
                catch (Exception ex)
                {
                    _logger?.AddLog(LOG_MODE.ERROR, $"StartBat,{ex.Message}");
                }
                finally
                {
                    _logger.AddLog(LOG_MODE.INFO, $"StartBat finished.");
                }
            }
        }
        private void btnRestartMain_Click(object sender, EventArgs e)
        {
            doFinishNormal();
        }
    }
}
