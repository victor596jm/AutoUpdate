﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
namespace BIN
{
    public class MyWebClient : System.Net.WebClient
    {
        private int timeOut=2000;
        /// <summary>
        /// 时间为毫秒
        /// </summary>
        /// <param name="timeOut"></param>
        public MyWebClient(int miliSeconds=2000)
        {
            this.timeOut = miliSeconds;
        }
        protected override WebRequest GetWebRequest(Uri address)
        {
            string url = address.ToString();
            HttpWebRequest request = null;
            if (!url.IsFtp())
            {
                request = (HttpWebRequest)base.GetWebRequest(address);
                request.UseDefaultCredentials = true;
            }
            else
            {
                string newUrl = "", user = "", pass = "";
                newUrl = Common.SplitFtpUrl(url, out user, out pass);
                request.Credentials = new NetworkCredential(user, pass);
                request = (HttpWebRequest)base.GetWebRequest(new Uri(newUrl));
            }
            request.Timeout = timeOut;
            request.ReadWriteTimeout = timeOut;
            return request;
        }
    }
}
