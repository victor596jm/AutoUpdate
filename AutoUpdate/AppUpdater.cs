﻿#define DOWN_PART //分包下载
#define AUTOFINISH //自动完成下载
//#define CHECK_MD5
//#define USE_DEFAULT_URL_WHEN_NULL  //当取不到URL时是否使用一个默认的连接地址
#define SILENT_MODE //此模式是YSPOS用的
/*
 *DELAY_UPDATE
 *延迟更新模式(不杀死主程序,主程序下次启动时从临时目录直接复制要更新的文件)
 *此模式无法执行服务进程的停止重启，也无法执行文件的重命名操作
 *此开关不可与SILENT_MODE同时定义
 */
//#define DELAY_UPDATE
/*
 *SILENT_MODE
 *静默更新模式
 *更新时不显示更新程序界面,更新完之后提示用户1分钟之后将重启主程序，也可以让用户立即重启
 *最好是检查时间，如果整个更新花费N分钟以上才提醒用户,否则按一般模式执行
 *此开关不可与DELAY_UPDATE同时定义
 */
using System;
using System.Web;
using System.IO;
using System.Net;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
namespace BIN
{
    class UpdateUrlSpeed
    {
        public string Url { get; set; }
        public int TestMSecond { get; set; }
        public bool IsDefault { get; set; }
        public UpdateUrlSpeed(string url)
            : this(url, 100000, false)
        {
        }
        public UpdateUrlSpeed(string url, int mSecond)
        {
            this.Url = url;
            this.TestMSecond = mSecond;
        }
        public UpdateUrlSpeed(string url, int mSecond, bool isDefault)
            : this(url, mSecond)
        {
            this.IsDefault = isDefault;
        }
    }
    class AppUpdater : IDisposable
    {
        readonly static AutoUpdateLog _logger = BIN.AutoUpdateLog.LogObj;
        /// <summary>
        /// 默认的下载地址,没有设置Url时
        /// </summary>
        const string DEFAULT_URL = "http://your_default_update_point/vf/";
        #region "Control Points"
        /// <summary>
        /// 提醒更新模式,不杀掉进程，等下次启动主进程后再覆盖文件
        /// </summary>
        public static bool RewakeUpdateMode
        {
            get
            {
#if DELAY_UPDATE
                return true;
#else
                return false;
#endif
            }
        }
        public static bool AutoFinish
        {
            get
            {
#if AUTOFINISH
                return true;
#else
                return false;
#endif
            }
        }
        public static bool PartialDowning
        {
            get
            {
#if DOWN_PART
                return true;
#else
            return false;
#endif
            }
        }
        public static bool UseDefaultUrl
        {
            get
            {
#if USE_DEFAULT_URL_WHEN_NULL
                return true;
#else
                return false;
#endif
            }
        }
        public static bool IsSilentMode(XmlFiles xmlFiles)
        {
            bool result = false;
            string str = xmlFiles.GetNodeValue("//Updater//SilentMode");
            if (!string.IsNullOrWhiteSpace(str))
            {
                result = str.IsTrue();
            }
            else
            {
#if  SILENT_MODE
                result = true;
#endif
            }
            return result;
        }
        public static readonly string DownloadTempDir = Common.CombineDir(Common.GetAppDir(), "temp");
        #endregion
        private string _svrFilePath = string.Empty, _clientFilePath = string.Empty;
        private bool disposed = false;
        /// <summary>
        /// 限制升级的KEY,匹配UserConfig.xml的项目UPDATE_KEY
        /// </summary>
        public string LimitKey { get; private set; }
        private XmlFiles _localXmlFile = null;
        /// <summary>
        /// 当前站点是否禁用升级
        /// </summary>
        /// <returns></returns>
        public bool CurrPointWasLimited()
        {
            //服务端没限制
            if (string.IsNullOrEmpty(this.LimitKey))
            {
                return false;
            }
            string[] arr = this.LimitKey.Split(new char[] { ',', ':', ';' }, StringSplitOptions.RemoveEmptyEntries);
            string currKey = Common.UpdateKey;
            return !arr.Contains(currKey, new StringCompareNoCase());
        }
        private readonly static int _md5Bits = 8; //取MD5值的前8位
        public string GetMD5HashFromFile(string fileName)
        {
            try
            {
                FileStream file = new FileStream(fileName, FileMode.Open);
                System.Security.Cryptography.MD5 md5 =
                    new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] retVal = md5.ComputeHash(file);
                file.Close();
                StringBuilder sb = new StringBuilder();
                int len = retVal.Length;
                if (0 != _md5Bits && len > _md5Bits) len = _md5Bits / 2;
                for (int i = 0; i < len; i++)
                {
                    sb.Append(retVal[i].ToString("x2"));
                }
                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }
        private ArrayList oldFileAl = new ArrayList();
        private ArrayList RenameFileLst = new ArrayList(); //需要改名的
        private string _ShortConfigName = "UpdateList.xml";
        public string ShortConfigName
        {
            get { return _ShortConfigName; }
            set { _ShortConfigName = value; }
        }
        public string AppDir = string.Empty;
        //private IntPtr handle;
        private Component component = new Component();
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);
        public string FileRenameTo(string FileName)
        {
            RenameFile renFile = null;
            for (int i = 0; i < RenameFileLst.Count; i++)
            {
                renFile = ((RenameFile)RenameFileLst[i]);
                if (renFile.OriFileName.Trim().ToUpper() == FileName.Trim().ToUpper())
                    return renFile.NewFileName;
            }
            return string.Empty;
        }
        /// <summary>
        /// 外部传入
        /// </summary>
        public string UpdateListUrl { get; internal set; }
        public string UpdateBaseUrl { get; internal set; }
        public void WriteException(Exception ex)
        {
            _logger.AddLog(LOG_MODE.ERROR, ex.Message + ex.Source + ex.StackTrace);
        }
        public static bool DealUrlTruncFlag(ref string url)
        {
            if (!string.IsNullOrWhiteSpace(url) && url.EndsWith(";OK")) //不要使用这种
            {
                url = url.Replace(";OK", string.Empty);
                return true;
            }
            return false;
        }
        public static string limitFile { get; set; } = "limit.xml";
        /// <summary>
        /// 返回下载文件的毫秒
        /// </summary>
        /// <param name="url"></param>
        /// <param name="tmpDir"></param>
        /// <returns></returns>
        private int GetUrlSpeed(string url, string tmpDir)
        {
            DealUrlTruncFlag(ref url);
            _logger.AddLog(LOG_MODE.INFO, $"Try to download {limitFile} from {url}' to '{tmpDir}'!");
            DateTime start = DateTime.Now, end;
            Common.Download(url, limitFile, tmpDir);
            end = DateTime.Now;
            if (Common.FileExists(Common.CombineDir(tmpDir, limitFile)))
            {
                TimeSpan st = new TimeSpan(start.Ticks);
                TimeSpan et = new TimeSpan(end.Ticks);
                return Math.Abs(et.Subtract(st).Duration().Milliseconds);
            }
            else
            {
                throw new Exception($"Error occurs when downloading file from {url} !");
            }
        }
        private bool IsUrlOK(string url, string tmpDir)
        {
            Common.Download(url, limitFile, tmpDir);
            return Common.FileExists(Common.CombineDir(tmpDir, limitFile));
        }
        private void DealUrl(string tmpDir, ref string url, out bool isDefault,
            out int speed, ref List<UpdateUrlSpeed> urls)
        {
            isDefault = DealUrlTruncFlag(ref url);
            if (!string.IsNullOrWhiteSpace(url))
            {
                speed = GetUrlSpeed(url, tmpDir);
                if (1000000 != speed)
                {
                    urls.Add(new UpdateUrlSpeed(url, speed, isDefault));
                    _logger.AddLog(LOG_MODE.INFO, $"Url:'{url}' Speed:{speed} ms!");
                }
            }
            else
            {
                throw new Exception("//Url is empty!");
            }
        }
        /// <summary>
        /// 根据测速结果取升级地址
        /// </summary>
        /// <param name="xmlobj"></param>
        /// <returns></returns>
        public string TestGetUpdateUrl()
        {
            List<UpdateUrlSpeed> urls = new List<UpdateUrlSpeed>();
            string tmpDir = Common.CombineDir(Common.GetAppDir(), "temp");
            Common.CheckFolderExists(tmpDir);
            string url = string.Empty;
            int speed = 0;
            bool isDefault = false;
            try
            {
                url = _localXmlFile.GetNodeValue("//Url");
                DealUrl(tmpDir, ref url, out isDefault, out speed, ref urls);
                if (isDefault) goto ExitAll;
            }
            catch (Exception ex)
            {
                speed = 10000000;
                WriteException(ex);
            }
            if (speed <= 200)
            {
                _logger.AddLog(LOG_MODE.INFO, $"Url: {url} speed:{speed}ms,It is a well line,no testing other lines necessary!");
            }
            else
            {
                try
                {
                    url = _localXmlFile.GetNodeValue("//Url1");
                    DealUrl(tmpDir, ref url, out isDefault, out speed, ref urls);
                    if (isDefault) goto ExitAll;
                }
                catch (Exception ex)
                {
                    speed = 100000;
                    WriteException(ex);
                }
                if (speed <= 200)
                {
                    _logger.AddLog(LOG_MODE.INFO, $"Url: {url} speed:{speed}ms,It is a well line,no testing other lines necessary!");
                }
                else
                {
                    try
                    {
                        url = _localXmlFile.GetNodeValue("//Url2");
                        DealUrl(tmpDir, ref url, out isDefault, out speed, ref urls);
                        if (isDefault) goto ExitAll;
                    }
                    catch (Exception ex)
                    {
                        speed = 1000000;
                        WriteException(ex);
                    }
                }
            }
            ExitAll:
            urls.Sort(delegate (UpdateUrlSpeed a, UpdateUrlSpeed b)
            {
                return a.TestMSecond.CompareTo(b.TestMSecond);
            });
            if (0 < urls.Count)
            {
                UpdateUrlSpeed defSite = urls.FirstOrDefault(c => c.IsDefault);
                if (null != defSite)
                {
                    url = defSite.Url;
                }
                else
                {
                    url = urls[0].Url;
                }
                DealUrlTruncFlag(ref url);
                _logger.AddLog(LOG_MODE.INFO, $"Choose the highest speed line :'{url}'!");
            }
            else
            {
#if USE_DEFAULT_URL_WHEN_NULL
                url = DEFAULT_URL;
#else
                url = string.Empty;
#endif
            }
            //获取LimitKey
            try
            {
                FillLimitKey(tmpDir);
            }
            catch (Exception ex)
            {
                WriteException(ex);
            }
            return url;
        }
        public void FillLimitKey(string tmpdir)
        {
            string limitFilePath = Path.Combine(tmpdir, limitFile);
            if (!File.Exists(limitFilePath)) return;
            try
            {
                this.LimitKey = XPathGet.GetValue(limitFilePath, "//Config//LimitKey");
            }
            catch
            {
                this.LimitKey = string.Empty;
            }
        }
        internal string[] KillProgams { get; set; } = null; //要杀掉的进程
        internal bool CheckMd5 { get; private set; } = false;
        internal bool NeedUpdate { get; private set; } = true;
        internal int DownloadingTimeout { get; private set; } = 500000;
        internal bool UseBat { get; private set; } = false;
        internal string StartBat { get; private set; }
        internal string StopBat { get; private set; }
        /// <summary>
        /// 就是没更新也要执行启用脚本，此功能可以检查非正式退出的程序，最好是服务，如果是非服务还要再作另外的处理
        /// </summary>
        internal string AlwaysExecStartBat { get; private set; }
        public AppUpdater(XmlFiles localXmlConfig)
        {
            _localXmlFile = localXmlConfig;
            string str = _localXmlFile.GetNodeValue("//KillPrograms");
            _logger.AddLog(LOG_MODE.INFO, $"KillPrograms,{str}");
            if (!string.IsNullOrWhiteSpace(str))
            {
                this.KillProgams = str.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
            }
            str = _localXmlFile.GetNodeValue("//Updater//CheckMd5");
            _logger.AddLog(LOG_MODE.INFO, $"CheckMd5,{str}");
            if (!string.IsNullOrWhiteSpace(str))
            {
                this.CheckMd5 = str.IsTrue();
            }
            else
            {
#if CHECK_MD5
                this.CheckMd5 = true;
#endif
            }
            str = _localXmlFile.GetNodeValue("//NeedUpdate");
            this.NeedUpdate = str.IsTrue();
            str = _localXmlFile.GetNodeValue("//DownloadTimeout");
            if (!string.IsNullOrWhiteSpace(str))
            {
                int timeout = 0;
                if (int.TryParse(str, out timeout) && timeout >= 1000)
                    this.DownloadingTimeout = timeout;
            }
            _logger.AddLog(LOG_MODE.INFO, $"downloading timeout :{this.DownloadingTimeout} ms.");
            var node = Common.Config.GetXmlNode("USE_BAT");
            if (null != node)
            {
                this.UseBat = node.Attributes["UseBat"].InnerText.IsTrue();
                this.StartBat = node.Attributes["StartBat"].InnerText;
                this.StopBat = node.Attributes["StopBat"].InnerText;
                this.AlwaysExecStartBat = node.Attributes["AlwaysExecStartBat"]?.InnerText;
                _logger.AddLog(LOG_MODE.INFO, $"UseBat?{UseBat},StartBat:{StartBat},StopBat:{StopBat}");
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    component.Dispose();
                }
            }
            disposed = true;
        }
        ~AppUpdater()
        {
            Dispose(false);
        }
        ///旧文件是否存在
        private bool ExistsFile(string FileName)
        {
            int Count = oldFileAl.Count;
            for (int i = 0; i < Count; i++)
            {
                if (((LocalFileVersion)oldFileAl[i]).FileName.Equals(
                    FileName, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }
            return false;
        }
        private string GetOldFileVersion(string FileName)
        {
            int Count = oldFileAl.Count;
            for (int i = 0; i < Count; i++)
            {
                if (((LocalFileVersion)oldFileAl[i]).FileName.ToString().Trim().Equals(
                    FileName.Trim(), StringComparison.OrdinalIgnoreCase))
                {
                    return ((LocalFileVersion)oldFileAl[i]).Version;
                }
            }
            return "0.0.0.0";
        }
        private bool ServerFileIsNewer(string FileName, string Version)
        {
            if (!(ExistsFile(FileName))) return true;
            string oldVersion = GetOldFileVersion(FileName);
            //return ( string.Compare(Version ,oldVersion ,true )!=0 );
            bool ret = ServerFileIsNewerPri(Version, oldVersion);
            if (ret)
            {
                _logger.AddLog(LOG_MODE.INFO,
                   string.Format("File '{0}'  server:{1},client:{2}",
                   FileName, Version, oldVersion));
            }
            return ret;
        }
        private bool ServerFileIsNewerPri(string svr, string local)
        {
            string svrNum = svr.Replace(".", string.Empty);
            string localNum = local.Replace(".", string.Empty);
            int ret_svr = 0, ret_local = 0;
            if (!int.TryParse(svrNum, out ret_svr)) ret_svr = 1;
            if (!int.TryParse(localNum, out ret_local)) ret_local = 0;
            return (ret_svr > ret_local);
        }
        public bool ServerConfigIsNewer()
        {
            if (string.IsNullOrEmpty(_clientFilePath)
                || string.IsNullOrEmpty(_svrFilePath)) return false;
            XmlFiles _svrObj = null, _cltObj = null;
            try
            {
                _svrObj = new XmlFiles(_svrFilePath);
                _cltObj = new XmlFiles(_clientFilePath);
                string svrVersion = _svrObj.GetNodeValue("//Version");
                string cltVersion = _cltObj.GetNodeValue("//Version");
                return (string.Compare(svrVersion, cltVersion, true) != 0);
            }
            catch
            {
                return false;
            }
            finally
            {
                if (null != _svrObj) _svrObj = null;
                if (null != _cltObj) _cltObj = null;
            }
        }
        private string GetAttrVal(XmlNode node, string attrName)
        {
            try
            {
                return node.Attributes[attrName].Value;
            }
            catch
            {
                return string.Empty;
            }
        }
        private static readonly Dictionary<string, string> md5OfFile = new Dictionary<string, string>();
        public string GetMd5(string fileName)
        {
            string ret = string.Empty;
            if (md5OfFile.TryGetValue(fileName, out ret)) return ret;
            return string.Empty;
        }
        /// 检查更新文件
        public int CheckForUpdate(string serverXmlFile, string localXmlFile, out Hashtable updateFileList)
        {
            _svrFilePath = serverXmlFile;
            _clientFilePath = localXmlFile;
            XmlFiles serverXmlFiles = null, localXmlFiles = null;
            int k = 0;
            updateFileList = new Hashtable();
            try
            {

                if (!File.Exists(localXmlFile) || !File.Exists(serverXmlFile)) return -1;
                serverXmlFiles = new XmlFiles(serverXmlFile);
                localXmlFiles = new XmlFiles(localXmlFile);
                XmlNodeList newNodeList = serverXmlFiles.GetNodeList(@"AutoUpdater/Files");
                XmlNodeList oldNodeList = localXmlFiles.GetNodeList(@"AutoUpdater/Files");
                string newFileName, newVer, newKeyUpt, renTo, oldFileName, oldVer, tmpFileName;
                for (int j = 0; j < oldNodeList.Count; j++)
                {
                    try
                    {
                        if (oldNodeList.Item(j).Attributes == null) continue;
                        if (oldNodeList.Item(j).Attributes.Count < 3) continue;
                        oldFileName = GetAttrVal(oldNodeList.Item(j), "Name");
                        tmpFileName = oldFileName.ToUpper().Trim();
                        if (Common.FileIsFixedExcepted(tmpFileName)) continue;
                        //排除文件
                        //if (oldFileName.Equals("abc.dll", StringComparison.OrdinalIgnoreCase)) continue;
                        if (clsExcept.Instance.FileWasExcepted(oldFileName))
                        {
                            _logger.AddLog(LOG_MODE.INFO, $"File '{oldFileName}' is exceptted!");
                            continue;//排除的文件
                        }
                        oldVer = GetAttrVal(oldNodeList.Item(j), "Ver");
                        oldFileAl.Add(new LocalFileVersion(oldFileName, oldVer));
                    }
                    catch (Exception ex)
                    {
                        _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                    }
                }
                for (int i = 0; i < newNodeList.Count; i++)
                {
                    if (newNodeList.Item(i).Attributes == null) continue;
                    if (newNodeList.Item(i).Attributes.Count < 3) continue;
                    string[] fileList = new string[4];
                    newFileName = GetAttrVal(newNodeList.Item(i), "Name");
                    tmpFileName = newFileName.ToUpper().Trim();
                    if (Common.FileIsFixedExcepted(tmpFileName)) continue;
                    //排除文件
                    //if (newFileName.Equals("abc.dll", StringComparison.OrdinalIgnoreCase)) continue;
                    if (clsExcept.Instance.FileWasExcepted(newFileName))
                    {
                        _logger.AddLog(LOG_MODE.INFO,
                            string.Format("File '{0}' is exceptted!", newFileName));
                        continue;//排除的文件
                    }
                    newVer = GetAttrVal(newNodeList.Item(i), "Ver");
                    newKeyUpt = "0";
                    renTo = string.Empty; //重命名为
                    try
                    {
                        newKeyUpt = GetAttrVal(newNodeList.Item(i), "IsKeyUpt"); //是否关键更新
                    }
                    catch (Exception ex)
                    {
                        newKeyUpt = "0";
                        _logger.AddLog(LOG_MODE.WARN, ex.Message);
                    }
                    try
                    {
                        renTo = GetAttrVal(newNodeList.Item(i), "RenameTo");
                    }
                    catch
                    {
                        renTo = string.Empty;
                    }
                    if (ServerFileIsNewer(newFileName, newVer))
                    {
                        fileList[0] = newFileName;
                        fileList[1] = newVer;
                        fileList[2] = string.Empty;
                        fileList[3] = (newKeyUpt == "1" ? "Y" : string.Empty);
                        updateFileList.Add(k, fileList);
                        string md5 = GetAttrVal(newNodeList.Item(i), "Md5");
                        if (!string.IsNullOrEmpty(md5))
                        {
                            md5OfFile.Add(newFileName, md5);
                        }
                        if (!string.IsNullOrEmpty(renTo))
                        {
                            RenameFileLst.Add(new RenameFile(newFileName, renTo));
                        }
                        k++;
                        /*
                        _logger.AddLog(LOG_MODE.INFO,
                            "File " + newFileName + " on server is newer!");
                        */
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                throw ex;
            }
            finally
            {
                if (null != serverXmlFiles) serverXmlFiles = null;
                if (null != localXmlFiles) localXmlFiles = null;
            }
            return k;
        }
        //检查当前版本
        public string GetVersion()
        {
            string localXmlFile = AppDir + this.ShortConfigName;
            XmlFiles XmlFile = new XmlFiles(localXmlFile);
            try
            {
                string Version = (XmlFile.GetNodeValue("//Version"));
                string VersionSort = (XmlFile.GetNodeValue("//VersionSort"));
                return VersionSort + " " + Version;
            }
            catch (Exception ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                return string.Empty;
            }
        }
        /// 返回下载更新文件的临时目录
        public void DownAutoUpdateFile(string downpath)
        {
            try
            {
                Common.Download(this.UpdateBaseUrl, this.ShortConfigName, downpath);
            }
            catch (Exception ex)
            {
                _logger.AddLog(LOG_MODE.ERROR, ex.Message);
                throw new Exception("error occurs when getting the config file from server(connection timeout)!");
            }
        }
    }
}
