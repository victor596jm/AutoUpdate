﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace BIN
{
    static class XPathGet
    {
        public static XmlNodeList GetNodeList(string FileName, string Xpath)
        {
            if (!Common.FileExists(FileName)) return null;
            XmlFiles xmlObj = null;
            try
            {
                xmlObj = new XmlFiles(FileName);
                return GetNodeList(xmlObj, Xpath);
            }
            finally
            {
                if (null != xmlObj) xmlObj = null;
            }
        }
        public static XmlNodeList GetNodeList(XmlFiles xmlObj, string Xpath)
        {
            return xmlObj.SelectNodes(Xpath);
        }
        public static XmlNode GetNode(string FileName, string Xpath)
        {
            if (!Common.FileExists(FileName)) return null;
            XmlFiles xmlObj = null;
            try
            {
                xmlObj = new XmlFiles(FileName);
                return GetNode(xmlObj, Xpath);
            }
            finally
            {
                if (null != xmlObj) xmlObj = null;
            }
        }
        public static XmlNode GetNode(XmlFiles xmlObj, string Xpath)
        {
            return xmlObj.SelectSingleNode(Xpath);
        }
        public static string GetValue(XmlFiles xmlObj, string Xpath, string Attribute)
        {
            XmlNode node = xmlObj.SelectSingleNode(Xpath);
            if (null == node) return string.Empty;
            if (string.IsNullOrEmpty(Attribute))
                return node.InnerText;
            else
                return node.Attributes[Attribute].InnerText;
        }
        public static string GetValue(string FileName, string Xpath ,string Attribute="")
        {
            if (!Common.FileExists(FileName)) return string.Empty;
            XmlFiles xmlObj = null;
            try
            {
                xmlObj = new XmlFiles(FileName);
                return GetValue(xmlObj, Xpath, Attribute);
            }
            finally
            {
                if (null != xmlObj) xmlObj = null;
            }
        }
    }
}
