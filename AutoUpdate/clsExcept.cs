﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
namespace BIN
{
    class clsExcept
    {
        private string _fileName = string.Empty;
        private List<string> _exceptLst = null;
        private void Init()
        {
             _fileName = Common.CombineDir(Common.GetAppDir(), "ExceptFile.xml");
            if (!Common.FileExists(_fileName)) return;
            _exceptLst = new List<string>();
            XmlFiles xmlobj = new XmlFiles(_fileName);
            XmlNodeList lst= xmlobj.SelectNodes(@"//Except//File");
            if (null != lst && lst.Count > 0)
            {
                for (int i = 0, count = lst.Count; i < count; i++)
                {
                    _exceptLst.Add(lst[i].InnerText.Trim());
                }
            }
            if (null != xmlobj) xmlobj = null;
        }
        public clsExcept()
        {
            Init();
        }
        public bool FileWasExcepted(string fileName)
        {
            if (null == _exceptLst || 
                0 == _exceptLst.Count) return false;
            return _exceptLst.Contains(fileName);
        }
        public static readonly clsExcept Instance = new clsExcept();
    }
}
