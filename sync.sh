#git pull;
#if [ "$?" = "0" ] ; then
output=$(git pull origin HEAD |grep "CONFLICT")
comment=$1
if [ "$comment" = "" ]; then
	comment="`date '+%Y%m%d%H%M'`"
fi
if [ "$output" != "" ] ; then
	echo "CONFLICT"
else
	git add . --all
	output=$( git commit -m "$comment")
	result=$(echo $output | grep "nothing to commit")
	if [ "$result" = "" ]; then
		git push origin HEAD:master
	else
		echo "nothing to commit,no push necessary"
	fi
fi
